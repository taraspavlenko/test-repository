﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ValueTest.Extensions;
using ValueTest.Model;
using ValueTest.Views;

namespace ValueTest.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserForListView>()
                .ForMember(dest => dest.PhotoUrl, opt =>
                {
                    opt.MapFrom(scr => scr.Photos.FirstOrDefault(x => x.IsMain).Url);
                })
                .ForMember(dest => dest.Age, opt =>
                {
                    opt.ResolveUsing(scr => scr.DateOfBirth.CaclulateAge());
                });
            CreateMap<User, UserForDetailedView>()
                .ForMember(dest => dest.PhotoUrl, opt =>
                {
                    opt.MapFrom(scr => scr.Photos.FirstOrDefault(x => x.IsMain).Url);
                })
                .ForMember(dest => dest.Age, opt =>
                {
                    opt.ResolveUsing(scr => scr.DateOfBirth.CaclulateAge());
                });
            CreateMap<Photo, PhotoUserForDetailedItemView>();
        }
    }
}
