﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ValueTest.Data.Interfaces;
using ValueTest.Model;
using ValueTest.Views;

namespace ValueTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthRepository _authRepository;
        private readonly IConfiguration _configuration;

        public AuthController(IAuthRepository authRepository, IConfiguration configuration)
        {
            _authRepository = authRepository;
            _configuration = configuration;
        }
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]UserForRegistrationView view)
        {
            view.UserName = view.UserName.ToLower();
            if (await _authRepository.UserExists(view.UserName))
                return BadRequest("User name already exists");
            var userToCreate = new User
            {
                UserName = view.UserName
            };
            var createdUser = await _authRepository.Register(userToCreate, view.Password);
            return StatusCode(201);
        }
        [HttpPost("login")]
        public async Task<IActionResult> Login(UserForLoginView userLoginView)
        {
            var userFromRepo = await _authRepository.Login(userLoginView.UserName, userLoginView.Password);
            if (userFromRepo == null)
                return Unauthorized();

            var claims = new[]
            {
                new Claim("nameId", userFromRepo.Id.ToString()),
                new Claim("unique_name", userFromRepo.UserName)

            };
            var key = new SymmetricSecurityKey(Encoding.UTF8
                .GetBytes(_configuration.GetSection("AppSettings:Token").Value));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            
            var token = new JwtSecurityToken(
                issuer: null,
                audience: null,
                claims: claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: credentials);

            var tokenHandler = new JwtSecurityTokenHandler();
            return Ok(new
            {
                token = tokenHandler.WriteToken(token)
            });
        }
    }
}