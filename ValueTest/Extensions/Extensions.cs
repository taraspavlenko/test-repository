﻿using System;

namespace ValueTest.Extensions
{
    public static class Extensions
    {
        public static int CaclulateAge(this DateTime dateTime)
        {
            var age = DateTime.Today.Year - dateTime.Year;
            if (dateTime.AddYears(age) > DateTime.Today)
                age--;

             return age;
        }
    }
}
