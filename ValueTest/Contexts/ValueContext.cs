﻿using Microsoft.EntityFrameworkCore;
using ValueTest.Model;

namespace ValueTest.Contexts
{
    public class ValueContext : DbContext
    {
        public ValueContext(DbContextOptions options):base(options) { }

        public DbSet<Value> Values{ get; set; }
        public DbSet<User> Users{ get; set; }
        public DbSet<Photo> Photos { get; set; }
    }
}
