﻿using System;
using System.Collections.Generic;

namespace ValueTest.Views
{
    public class UserForDetailedView
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public string KnownAs { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastActive { get; set; }
        public string Introduction { get; set; }
        public string LookingFor { get; set; }
        public string Interests { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string  PhotoUrl { get; set; }
        public ICollection<PhotoUserForDetailedItemView> Photos { get; set; }
    }

    public class PhotoUserForDetailedItemView
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public bool IsMain { get; set; }
    }
}
