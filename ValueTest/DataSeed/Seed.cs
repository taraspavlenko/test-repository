﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ValueTest.Contexts;
using ValueTest.Model;

namespace ValueTest.DataSeed
{
    public class Seed
    {
        private readonly ValueContext _context;

        public Seed(ValueContext context)
        {
            _context = context;
        }

        public void SeedUser()
        {
            if (!_context.Users.Any())
            {
                var userDate = File.ReadAllText("DataSeed/UserSeed.json");
                var users = JsonConvert.DeserializeObject<List<User>>(userDate);
                foreach (var user in users)
                {
                    byte[] passwordHash;
                    byte[] passwordSalt;
                    CreatePasswordHash("password", out passwordHash, out passwordSalt);
                    user.PasswordHash = passwordHash;
                    user.PasswordSalt = passwordSalt;
                    user.UserName = user.UserName.ToLower();
                }
                _context.AddRange(users);
                _context.SaveChanges();
            }
        }
        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
    }
}
